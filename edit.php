<?php
session_start();
if (isset($_SESSION['id'])){
    $userId = $_SESSION['id'];
    $username = $_SESSION['username'];
}
else {
    header('Location: index.php');
    die();
}

include ('headers/nav.php');

?>
<?php

include ('product/Product.php');

$product = new Product();

$resultado = $product->show($_REQUEST[id]);

while ($fila = $resultado->fetch_assoc()) {

    ?>
    <br><br>
    <div class="container">
       <div class="row">
           <form action="product/updat.php" method="post">
               <div class="row">
                   <div class="col s12">
                       <div class="row">
                           <div class="input-field col s12 l6">
                               <i class="material-icons prefix">account_circle</i>
                               <input id="icon_prefix" type="text" class="validate" name="nombre" value="<?php echo $fila["nombre"]?>">
                               <label for="icon_prefix">Nombre</label>
                           </div>
                           <div class="input-field col s12 l6">
                               <i class="material-icons prefix">account_circle</i>
                               <input id="icon_telephone" type="tel" name="precio" class="validate" value="<?php echo $fila["precio"]?>">
                               <label for="icon_telephone">Precio</label>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col s12">
                       <div class="row">
                           <div class="input-field col s12 ">
                               <i class="material-icons prefix">account_circle</i>
                               <input id="icon_prefix" type="text" class="validate" name="descripcion" value="<?php echo $fila["descripcion"]?>">
                               <label for="icon_prefix">Descripcion</label>
                           </div>
                       </div>
                   </div>
               </div>
               <input type="hidden" name="id" value="<?php echo $fila["id"] ?>">
               <input type="submit" name="logout" class="btn btn-large col s12" value="Guardar Cambios">
           </form>
       </div>
    </div>

    <?php
}
?>

</body>
</html>