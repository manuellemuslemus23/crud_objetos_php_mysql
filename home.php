<?php
    session_start();
    if (isset($_SESSION['id'])){
        $userId = $_SESSION['id'];
        $username = $_SESSION['username'];
    }
    else {
        header('Location: index.php');
        die();
    }

    include ('headers/nav.php')
?>
    <div class="container">

        <h3>Bienvenido <b> <?php echo $username; ?> </b></h3>
        <form action="search.php" method="post">
            Ingrese nombre o precio del producto
            <input type="text" name="id">
            <div class="row">
                <input type="submit" class="btn col s12 btn-large" value="buscar">
            </div>
        </form>
        <table class="centered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Descripcion</th>
                <th colspan="2">Opciones</th>
            </tr>
            </thead>
            <tbody>
    <?php

    include ('product/Product.php');

    $product = new Product();

    $resultado = $product->Index();

    while ($fila = $resultado->fetch_assoc()) {
        ?>

            <tr>
                <td><?php echo $fila["nombre"]; ?></td>
                <td><?php echo $fila["precio"]; ?></td>
                <td><?php echo $fila["descripcion"]; ?></td>
                <td>
                    <div class="row">
                        <form action="edit.php" class="col s6">
                            <input type="hidden" name="id" value="<?php echo $fila['id'] ?>">
                            <input type="submit" class="btn teal" value="Editar">
                        </form>
                        <form action="product/delete.php" class="col s6">
                            <input type="hidden" name="id" value="<?php echo $fila['id'] ?>">
                            <input type="submit" class="btn red" value="Eliminar"  onclick="return confirm('esta seguro que desea eliminar ha : ' + '<?php echo $fila["nombre"]; ?> ' )">
                        </form>
                    </div>
                </td>
            </tr>

        <?php
    }
    ?>
            </tbody>
        </table>
        </div>
    </body>
</html>