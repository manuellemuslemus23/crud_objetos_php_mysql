<?php
session_start();
if (isset($_SESSION['id'])){
    $userId = $_SESSION['id'];
    $username = $_SESSION['username'];
}
else {
    header('Location: index.php');
    die();
}
include ('headers/nav.php')

?>
    <div class="container">
        <br><br>
        <div class="row">
            <form action="product/store.php">
            <div class="row">
                <div class="col s12">
                    <div class="row">
                        <div class="input-field col s12 l6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix" type="text" class="validate" name="nombre">
                            <label for="icon_prefix">Nombre</label>
                        </div>
                        <div class="input-field col s12 l6">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_telephone" type="tel" name="precio" class="validate">
                            <label for="icon_telephone">Precio</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="row">
                        <div class="input-field col s12 ">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix" type="text" class="validate" name="descripcion">
                            <label for="icon_prefix">Descripcion</label>
                        </div>
                    </div>
                </div>
            </div>
                <input type="submit" class="btn btn-large col s12" value="Guardar">
            </form>
        </div>
    </div>

    </body>
</html>