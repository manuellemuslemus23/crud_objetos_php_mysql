
<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<div class="container row">
    <h1 class="center">Login</h1>
    <div class="col s3"></div>
    <div class="center col s6">
        <form method="post" action="user/login.php" >
            <input type="text" name = "username"  placeholder="Nombre de Usuario">
            <input type="password" name="password" placeholder="Contraseña">
            <br><br>
            <input type="submit" name="submit" class="btn btn-large col s12" value="Login">
        </form>
        <a href="register.php" class="left" >Register</a>
    </div>
    <br><br>
</div>

</body>
</html>