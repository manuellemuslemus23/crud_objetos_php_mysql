<?php
session_start();
if (isset($_SESSION['id'])){
    $userId = $_SESSION['id'];
    $username = $_SESSION['username'];
}
else {
    header('Location: index.php');
    die();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Welcome user</title>
</head>
<body>
<h3>Welcome <?php echo $username; ?>.
    <br>
<?php

//include('connection.php');
include ('product/Product.php');

$product = new Product();

$resultado = $product->search($_REQUEST[id]);

while ($fila = $resultado->fetch_assoc()) {
    echo $fila['id'] . " - ";

    echo $fila["nombre"]. "<br>";
    ?>
    <form action="edit.php">
        <input type="hidden" name="id" value="<?php echo $fila['id'] ?>">
        <input type="submit" value="Editar">
    </form>
    <form action="product/delete.php">
        <input type="hidden" name="id" value="<?php echo $fila['id'] ?>">
        <input type="submit" value="Eliminar">
    </form>
    <?php
}
?>

<form action="user/logout.php">
    <input type="submit" name="logout" value="Logout">
</form>

</body>
</html>